//them so vao mang
var array = [];
function addNumberToArray() {
  var number = document.getElementById("txt-nhap-n").value * 1;
  array.push(number);
  document.getElementById("kqMang").innerHTML = array;
}
//lab 1
function sum() {
  var sum = 0;
  for (var i = 0; i < array.length; i++) {
    if (array[i] > 0) {
      sum += array[i];
    }
  }
  document.getElementById("kqBai1").innerHTML = `Tổng: ${sum}`;
}
// lab 2
function countNumber() {
  var count = 0;
  for (var i = 0; i < array.length; i++) {
    if (array[i] > 0) {
      count++;
    }
  }
  document.getElementById("kqBai2").innerHTML = `Số dương: ${count}`;
}
//lab 3
function findMinNumber() {
  var min = array[0];
  for (var i = 0; i < array.length; i++) {
    if (array[i] < min) {
      min = array[i];
    }
  }
  document.getElementById("kqBai3").innerHTML = `Số nhỏ nhất: ${min}`;
}
//lab 4
function findMinPositiveNumber() {
  var minPositive = array[0];
  for (var i = 0; i < array.length; i++) {
    if (array[i] < minPositive && array[i] > 0) {
      minPositive = array[i];
    }
  }
  document.getElementById(
    "kqBai4"
  ).innerHTML = `Số dương nhỏ nhất: ${minPositive}`;
}
//lab 5
function findLastEvenNumber() {
  var evenArray = [];
  for (var i = 0; i < array.length; i++) {
    if (array[i] % 2 == 0) {
      evenArray.push(array[i]);
    }
  }
  var lastEventNumber = evenArray[evenArray.length - 1];
  document.getElementById(
    "kqBai5"
  ).innerHTML = `Số chẵn cuối cùng: ${lastEventNumber}`;
}
// lab 6
function changeLocate() {
  var number1 = document.getElementById("nhap1").value * 1;
  var number2 = document.getElementById("nhap2").value * 1;
  var temp = array[number1];
  array[number1] = array[number2];
  array[number2] = temp;

  document.getElementById("kqBai6").innerHTML = `Mảng sau khi đổi: ${array}`;
}
//lab 7
function sortArray() {
  for (var i = 0; i < array.length; i++) {
    for (var j = 0; j < array.length - 1; j++) {
      if (array[j] > array[j + 1]) {
        var temp = array[j];
        array[j] = array[j + 1];
        array[j + 1] = temp;
      }
    }
  }
  document.getElementById(
    "kqBai7"
  ).innerHTML = `Mảng sau khi sắp xếp: ${array}`;
}
// lab 8
function isPrimeNumber(n) {
  if (n < 2) {
    return false;
  }
  var squareRoot = Math.sqrt(n);
  for (var i = 2; i <= squareRoot; i++) {
    if (n % i == 0) {
      return false;
    }
  }
  return true;
}
console.log(isPrimeNumber(4));
function findPrime() {
  for (var i = 0; i < array.length; i++) {
    if (isPrimeNumber(array[i]) == true) {
      document.getElementById("kqBai8").innerHTML = `${array[i]}`;
      break;
    }
  }
}
//lab 9
var array1 = [];
var array2 = [];
function addNumberToArray1() {
  var number1 = document.getElementById("nhap-n-bai9").value * 1;
  array1.push(number1);
  array2 = array.concat(array1);
  document.getElementById("kqMangBai9").innerHTML = array1;
  document.getElementById("kqMangBai9New").innerHTML = array2;
}
function countPrime() {
  var count = 0;
  for (var i = 0; i < array2.length; i++) {
    if (Number.isInteger(array2[i]) == true) {
      count++;
    }
  }
  document.getElementById("kqBai9").innerHTML = `Số nguyên: ${count}`;
}
//lab 10
function compareNumber() {
  var soAm = 0;
  var soDuong = 0;
  var result = "";
  for (var i = 0; i < array.length; i++) {
    if (array[i] > 0) {
      soDuong++;
    }
    if (array[i] < 0) {
      soAm++;
    }
  }
  if (soDuong > soAm) {
    result = "Số dương > Số âm";
  } else if (soDuong < soAm) {
    result = "Số dương < Số âm";
  } else {
    result = "Số dương = Số âm";
  }
  document.getElementById("kqBai10").innerHTML = result;
}
